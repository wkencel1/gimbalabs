---
id: 'gl-site'
title: 'gimbalabs.com site'
date: '2021-03-08'
tags: ['help wanted', 'in progress']
author: 'James Dunseith'
budget: 10000
---      

# gimbalabs.com

This site is a teaching tool and should make it easy for users to find ways to get involved. We invite anyone who is interested to help contribute to it.

## Prerequisites
Before we actively launch the project of asking for help on this project, we are:
1. Finishing up our initial set of brand assets to be used site-wide
2. Launching version 0.1 of the Gimbal Token

We're eager to test out some incentive structures by posting public tickets for tasks that need to be done around here, and will announce projects as soon as the prerequisites are complete.
