---
id: 'gl-init-01'
title: 'Experiment #1: Video Content Library'
date: '2021-03-08'
tags: ['initiative', 'in progress']
author: 'James Dunseith'
budget: 5000
---      

## Gimbalabs Initiatives
# Experiment #1: Video Content Library

### Project Leads
@mabloq and @farouk858

We are creating a library of short video content. We understand that a lot of people learn from short online videos, and we wonder if we can use the medium to inspire people to dig deeper into big, new ideas.

[The first video is on YouTube](https://youtu.be/6TRMOF0MiL4), and we'll be adding more in the coming weeks.
