---
id: 'gl-init-03'
title: 'Experiment #3: Content Translation'
date: '2021-03-09'
tags: ['initiative', 'help wanted', 'not started']
author: 'James Dunseith'
budget: 5000
---      

## Gimbalabs Initiatives
# Experiment #3: Content Translation

For our next initiative, we will begin to create systems for translating Gimbalabs content to languages other than English.

We have not yet launched this project, and we're eager to get the necessary systems in place to make time and organize people to deliver on it.

