---
id: 'playground-01'
title: 'Playground Project #1: Event Ticketing Service'
date: '2021-03-05'
tags: ['playground', 'in progress']
author: 'James Dunseith'
budget: 15000
---      

## Gimbalabs Playground
# Event Ticketing Service

- Take a look at [this project on GitLab](https://gitlab.com/gimbalabs/playground/event-ticketing)
- Take a look at [our public Miro board](https://miro.com/app/board/o9J_lR3E3vw=/)
- Join the project by [dropping by on Discord](https://discord.gg/JqdGXQ7p9U). Look for the channel: #001-event-ticketing-system