---
id: 'gl-init-02'
title: 'Experiment #2: Token-Based Incentive Structures'
date: '2021-03-08'
tags: ['initiative', 'in progress']
author: 'James Dunseith'
budget: 5000
---      

## Gimbalabs Initiatives
# Experiment #2: Token-Based Incentive Structures

### We started a very open-ended experiment in defining a token-based incentive structure for Gimbalabs. We started with the following assumptions:
1. Gimbalabs will be a DAO that creates educational content, connects builders, and consults with new and legacy organizations who want to learn about or build upon the blockchain.
2. A wide variety of people, with different skills, learning goals, individual projects, and potential time commitments will participate in the Gimbalabs DAO.
3. The income for this DAO will come from clients who hire teams to complete projects in blockchain adoption, use, and education.

### From these, we made this hypothesis...
We can use native tokens on Cardano to create incentive structures that reward people who contribute to educational, open source, and commercial goals of Gimbalabs. What we create will serve the needs of this DAO, while also providing lessons and experience that we can all share widely.

### and we asked our community for this "deliverable":
- A set of recommendations for how to build Gimbabals incentive structures.

What we got from our community was a far-reaching, invigorating conversation. You can see part of it on the Gimbalabs Discord in the #ex2-tokenomics-research channel. What we didn't quite reach is a clear set of recommendations. What we learned is that as an "initiative," this one was a bit too vague to lead to a real outcome.

## Next Steps
So what we're going to do is test some ideas, within safe bounds, for what the gimbal token can be. You can read about [our current plans on this GitLab repository](https://gitlab.com/gimbalabs/initiatives/ex2-tokenomics-research). Please share your feedback!

Our present step is to get Gimbal Pool up and running. When that's ready, we'll share more on the status of Gimbalabs treasury and our intentions for running experiments with our current funds.

