import Link from 'next/link'
import { TitleHeading1, Paragraph } from '../../../components/Type'

export default function nativeassets() {
    return (
        <div className="p-12">
            <TitleHeading1>
                Exploring Transactions and Native Assets on Cardano
            </TitleHeading1>
            <Paragraph>
                Running experiments
            </Paragraph>
        </div>
    )
}